��          �      �       0      1     R  
   Z      e     �     �  e   �  �     Q   �       -   (     V  �   k     e     }     �     �     �     �  ]   �  8   ?  q   x     �  -   �     '                                             	         
    /* Enter Your Custom CSS Here */ Add CSS Custom CSS Custom CSS updated successfully. John Regan, Danny Van Kooten Simple Custom CSS Simple Custom CSS allows you to add your own styles or override the default CSS of a plugin or theme. The simple, solid way to add custom CSS to your WordPress website. Simple Custom CSS allows you to add your own styles or override the default CSS of a plugin or theme. To use, enter your custom CSS, then click "Update Custom CSS".  It's that simple! Update Custom CSS http://johnregan3.github.io/simple-custom-css http://johnregan3.me PO-Revision-Date: 2016-05-10 12:24:27+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.1.0-alpha
Project-Id-Version: Development (trunk)
 /* Skriv din CSS her */ Tilføj CSS Brugerdefineret CSS Ændringerne er gemt John Regan, Danny Van Kooten Simple Custom CSS Simple Custom CSS tillader dig, at tilføje eller overskrive CSS fra et plugin eller et tema. Simpel og solid måde, at tilføje eller overskrive CSS. Det eneste du skal gøre, er at skrive din CSS og efterfølgende trykke på "Gem ændringer". Det er så simpelt! Gem ændringer http://johnregan3.github.io/simple-custom-css http://johnregan3.me 