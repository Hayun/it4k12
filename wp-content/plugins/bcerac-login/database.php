<?php
class SDatabase {
	/**
	* Query to be executed
	*
	* @var string
	*/
	private $_query 	= null;
	/**
	* The databade object
	*
	* @var mixed
	*/
	private $_resource 	= null;
	/**
	* Error messages
	*
	* @var string
	*/
	private $_error		= null;

	/**
	* Constructor
	*	- Connect and set the database
	*/
	public function __construct($host, $user, $password, $database) {		
		if(!($this->_resource = mssql_connect($host, $user, $password))) {
			$this->_error = "Error connecting to the Database";
			return;
		}
		if(empty($database) || !mssql_select_db("$database")) {
			die("Unable to select database {$database}: ".mssql_get_last_message());
		}
	}

	/**
	* Close all connections
	*
	*/
	public function __destruct() {
		if(is_resource($this->_resource))
			return mssql_close($this->_resource);
		return false;
	}

	/**
	* Get an instance of the database
	*
	* @return Database The database object
	*/
	public static function getInstance() {
		static $instance;

		if(empty($instance))
			$instance = new SDatabase();

		return $instance;
	}

	/**
	* Query the database
	*
	* @param mixed $query The query to execute
	* @return mixed False on failure
	*/
	public function query( $query = null ) {
		if(!is_resource( $this->_resource )){
			$this->_error = "No database object.";
			return false;
		}

		if(!is_null($query)) {
			$this->_query = $query;
		}

		if(!($result = mssql_query($this->_query, $this->_resource))) {
		    $this->_error = mssql_get_last_message();
		}

		return $result;
	}

	/**
	* Get the last insterted ID
	*
	*/
	public function insert( $query ) {
		$query = "SET NOCOUNT ON $query SELECT scope_identity() SET NOCOUNT OFF";

		return $this->fetchValue( $query );
	}

	/**
	* Grabs the next avaliable row from the result resource
	*
	* @param mixed $query Query to execute
	* @return array
	*/
	public function fetchArray( $query = null ) {
		if(!($tmpQuery = $this->query( $query )))
			return null;

		$ret = array();
		if( is_resource( $tmpQuery ) && $row = mssql_fetch_array( $tmpQuery, MSSQL_ASSOC ) )
			$ret = $row;

		mssql_free_result($tmpQuery);

		return $ret;
	}

	/**
	*
	*
	* @param mixed $query Query to execute
	* @param mixed $col	  Column to fetch
	* @return mixed
	*/
	public function fetchValue( $query = null, $col = 0) {
		if(!($tmpQuery = $this->query( $query )))
			return null;

		$ret = array();
		if( is_resource( $tmpQuery ) && $row = mssql_fetch_array( $tmpQuery ) )
			$ret = $row[$col];

		mssql_free_result($tmpQuery);

		return $ret;
	}

	/**
	* Get an array of results
	*
	* @param mixed $query Query to execute
	* @return array The results of the query
	*/
	public function fetchMulti( $query = null ) {
		if(!($tmpQuery = $this->query( $query )))
			return null;

		$out = array();

		while( $row = mssql_fetch_array( $tmpQuery, MSSQL_ASSOC) ) {
			$out[] = $row;
		}

		mssql_free_result($tmpQuery);
		return $out;
	}
	
	/**
	* Get a single-dimensional array of results in an optionally specified column (either numerically or associatively)
	*
	* @param mixed $query Query to execute
	* @param mixed $column The column to return (can be numeric offset or associative field name)
	* @return array The results of the query
	*/
	public function fetchColumn( $query = null, $column = 0 ) {
		if(!($tmpQuery = $this->query( $query )))
			return null;

		$out = array();

		while( $row = mssql_fetch_array( $tmpQuery ) ) {
			$out[] = $row[$col];
		}

		mssql_free_result($tmpQuery);
		return $out;
	}

	/**
	* Get the error messages
	*
	*/
	public function getError() {
		return $this->_error;
	}

	/**
	* Escape string
	*
	* @param string $value String to escape
	* @return string Escaped string
	*/
	public function escape( $value ) {
		$output = null;
		if(is_object($value) || is_array($value)) {
			$output = array();
			foreach($value as $key => $val) {
				$output[$key] = $this->escape($val);
			}
		} else {
			$output = str_replace("'", "''", (string)$value);
		}
		return $output;
	}


	public function rowsAffected() {
		return mssql_rows_affected($this->_resource);
	}
}