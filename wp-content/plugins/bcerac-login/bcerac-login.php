<?php
/*
Plugin Name: BCERAC MSSQL Login
Description: Connects to the MSSQL server to allow login with .NET credentials
Version: 1.0
Author: Techtone
Author URI: http://techtone.ca
*/

require_once 'database.php';

if(!function_exists('wp_authenticate')):
/**
 * Checks a user's login information and logs them in if it checks out.
 *
 * @since 2.5.0
 *
 * @param string $username User's username
 * @param string $password User's password
 * @return WP_User|WP_Error WP_User object if login successful, otherwise WP_Error object.
 */
function wp_authenticate($username, $password) {
	$username = sanitize_user($username);
	$password = trim($password);
	$dbserver="191.239.4.7:1433";
	$dbusername="sa";
	$dbpassword="Erac2011";
	$defaultdb="EIS_RELEASE_COMBINE";
	$db = new SDatabase($dbserver, $dbusername, $dbpassword, $defaultdb);
	
	/**
	 * Filter the user to authenticate.
	 *
	 * If a non-null value is passed, the filter will effectively short-circuit
	 * authentication, returning an error instead.
	 *
	 * @since 2.8.0
	 *
	 * @param null|WP_User $user     User to authenticate.
	 * @param string       $username User login.
	 * @param string       $password User password
	 */
	$user = apply_filters( 'authenticate', null, $username, $password );

	if ( $user == null ) {
		// TODO what should the error message be? (Or would these even happen?)
		// Only needed if all authentication handlers fail to return anything.
		$user = new WP_Error('authentication_failed', __('<strong>ERROR</strong>: Invalid username or incorrect password.'));
	}

	$ignore_codes = array('empty_username', 'empty_password');

	if (is_wp_error($user) && !in_array($user->get_error_code(), $ignore_codes) ) {
		$netUser = $db->fetchArray('SELECT * FROM MainLoginTable JOIN tb_Users ON tb_Users.MainLoginTableId = MainLoginTable.MainLoginTableId WHERE MainLoginTable.email LIKE \''.$db->escape($username).'\'');
		if($netUser) {
			$passParts = explode(':', $netUser['password']);
			$iterations = $passParts[0];
			$salt = base64_decode($passParts[1]);
			$goodHash = base64_decode($passParts[2]);
			$testHash = hash_pbkdf2("sha256", $password, $salt, $iterations, strlen($goodHash), true);
			$diff = strlen($testHash) ^ strlen($goodHash);
			for($i = 0; $i < strlen($testHash) && $i < strlen($goodHash); $i++) {
				$diff = $diff | $testHash[$i] ^ $goodHash[$i];
			}
			
			if($diff === 0) {
				$data = new stdClass;
				$data->first_name = $netUser['first_name'];
				$data->last_name = $netUser['last_name'];
				$data->user_login = $netUser['email'];
				$data->user_pass = $netUser['password'];
				$data->user_nicename = $netUser['first_name'] . ' ' . $netUser['last_name'];
				$data->user_email = $netUser['email'];
				$data->user_registered = $netUser['DateCreated'];
				$data->display_name = $netUser['first_name'] . ' ' . $netUser['last_name'];
				$data->description = $netUser['bio'];
				$data->role = 'subscriber';
				
				if(!($user_id = username_exists( $username ))) {
					$user_id = wp_insert_user($data);
				} else {
					wp_update_user($data);
				}
				
				$data->ID = $user_id;
				
				$user = new WP_User($data);
				
				return $user;
			}
		}
		
			/**
			 * Fires after a user login has failed.
			 *
			 * @since 2.5.0
	 		 *
			 * @param string $username User login.
			 */
			 do_action( 'wp_login_failed', $username );
	}

	return $user;
}
endif;