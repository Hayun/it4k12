<?php
	/*	
	*	Goodlayers Session File
	*/	
	
	function gdlr_get_session_speaker_list($speakers){
		$speaker_posts = array();
		
		if( !empty($speakers) ){
			foreach( $speakers as $speaker_name ){
				$speaker = get_posts(array('post_type'=>'speaker', 'name'=>$speaker_name, 'showposts'=>1));
				$speaker_posts[] = $speaker[0];
			}
		}
		return $speaker_posts;
	}
	
	function gdlr_get_session_thumbnail($size = 'thumbnail'){
		return '';
		$image_id = get_post_thumbnail_id();
		if( !empty($image_id) ){
			if( is_single() ){
				return '<div class="gdlr-session-thumbnail">' . gdlr_get_image($image_id, $size, true) . '</div>';
			}else{
				$ret  = '<div class="gdlr-session-thumbnail"><a href="' . get_permalink() . '" >';
				$ret .= gdlr_get_image($image_id, $size);
				$ret .= '</a></div>';
				return $ret;
			}
		}
		return '';
	}	
	
	function gdlr_session_time_conversion($time){
		global $theme_option;
		
		if( !empty($theme_option['time-format']) ){
			preg_match_all( "/(2[0-3]|[0-1]?[0-9]):[0-5]?[0-9]/", $time, $matches);
			
			foreach($matches[0] as $time24){
				$time12 = date("g:i a", strtotime($time24));
				$time = str_replace($time24, $time12, $time);
			}
		}
		return $time;
	}
	
	function gdlr_get_session_info($array = array(), $option=array(), $speakers=array(), $wrapper = true){ 
		// font awesome icon
		global $theme_option;
		$currentPage = get_queried_object();
		$currentPageSlug = $currentPage->post_name;
		if( !empty($theme_option['new-fontawesome']) && $theme_option['new-fontawesome'] == 'enable' ){
			$icon_class = array('time'=>'fa-clock-o', 'document'=>'fa-file-pdf-o');
		}else{
			$icon_class = array('time'=>'icon-time', 'document'=>'icon-download');
		}	
	
		// Add to Calendar Variables
		
		$fullTime=explode("-", $option['session-time']);
		$startTime=trim($fullTime[0]);
		$endTime=trim($fullTime[1]);
		
		// END Add to Calendar Variables

		require_once dirname(__FILE__) . "/../include/session/session-track.php";
		if(!isset($techtoneSessionTrack)) {
			$techtoneSessionTrack = new Techtone_Session_Track;
		}

		$ret = '';
		
		foreach($array as $post_info){	
			switch( $post_info ){
				case 'date':
					if(empty($option['session-time'])) break;
					$session_date = strtotime($option['session-date'] . ' 00:00:00');
			
					$ret .= '<div class="session-info session-time">';
					$ret .= '<i class="fa ' . gdlr_fa_class('icon-calendar') . '" ></i>';
					$ret .= date_i18n($theme_option['date-format'], $session_date);						
					$ret .= '</div>';						
				
					break;				
				case 'time':
					if(empty($option['session-time'])) break;
				
					$ret .= '<div class="session-info session-time">';
					$ret .= '<i class="fa ' . $icon_class['time'] . '" ></i>';
					$ret .= gdlr_session_time_conversion($option['session-time']);						
					$ret .= '</div>';						
				
					break;	
				case 'location':
					if(empty($option['location'])) break;

					$ret .= '<div class="session-info session-location">';
					$ret .= '<i class="fa ' . gdlr_fa_class('icon-location-arrow') . '" ></i>';
					$ret .= $option['location'];						
					$ret .= '</div>';	
					
					if(!in_array("no-track", $array)) {
						// Techtone: Add Session Track
						if(isset($option['session-track'])) {
							$techtoneSessionTrack->setTrackId($option['session-track']);
							if($techtoneSessionTrack->getTrackValue()) {					
							$ret .= '<div class="session-info session-track" style="color:' . $techtoneSessionTrack->getTrackColor() . '">';
							$ret .= '<i style="color:#8b8b8b" class="fa ' . gdlr_fa_class('fa-info-circle') . '" ></i>';
							$ret .= $techtoneSessionTrack->getTrackValue();						
							$ret .= '</div>';
							// Techtone: End Session Track
							}
						}
					}
					if($option['session-type'] != 'break' ){
					// Add to Calendar Code
					$ret .= '
						<div class="session-info session-speaker">
							<div class="session-speaker-inner">
								<i class="fa fa-calendar-o"></i> <a href="http://it4k12-2015.bcerac.ca" title="Add to Calendar" class="addthisevent" class="addthisevent-drop" style="color:#9ec63b;">
							    Add to Calendar
							    <span class="_start" style="display:none">'.$option['session-date'].' '. $startTime . '</span>
							    <span class="_end" style="display:none">'.$option['session-date'] .' '. $endTime . '</span>
							    <span class="_zonecode" style="display:none">6</span>
							    <span class="_summary" style="display:none">' .  get_the_title() . '</span>
							    <span class="_description" style="display:none"> ' . preg_replace('/<a.*>Read More<\/a>/','',get_the_excerpt()) . '</span>
							    <span class="_location" style="display:none">' . $option['location']  . ', 7551 Westminster Highway, Richmond, BC V6X 1A3</span>
							    <span class="_organizer" style="display:none">ERAC</span>
							    <span class="_organizer_email" style="display:none">info@bcerac.ca</span>
							    <span class="_all_day_event" style="display:none">false</span>
							    <span class="_date_format" style="display:none">DD/MM/YYYY</span>
								</a>
							</div>
						</div>'; 						
					// End Add to Calendar Code							
					}
					break;	
				case 'speaker':
					if(empty($speakers) || $speakers[0] == "" || $currentPageSlug === 'program') break;
					
					$ret .= '<div class="session-info session-speaker">';
					$ret .= '<div class="session-speaker-inner">';
					$ret .= '<i class="fa ' . gdlr_fa_class('icon-user') . '" ></i>';
					$ret .= '<div class="session-speaker-list">';
					foreach($speakers as $speaker){
						$speakerOption = get_post_meta($speaker->ID,'post-option');
						$speakerOption = json_decode($speakerOption[0]);

						$ret .= '<div class="session-speaker-list-item">';
						$ret .= '<a href="' . get_permalink($speaker) . '" style="color:#a7cf39;">';
						$ret .= $speakerOption->fullname?$speakerOption->fullname:get_the_title($speaker);						
						$ret .= '</a>';	
						$ret .= '</div>';
					}
					$ret .= '</div>'; // session-speaker-list				
					$ret .= '</div></div>'; // session-speaker-inner					
					
					break;	

				case 'document':
					if(empty($option['document-link'])) break;
				
					$ret .= '<div class="session-info session-document">';
					$ret .= '<a href="' . $option['document-link'] . '" target="_blank" >';
					$ret .= '<i class="fa ' . $icon_class['document'] . '" ></i>';
					$ret .= __('Download Document' ,'gdlr-conference');
					$ret .= '</a>';					
					$ret .= '</div>';						
				
					break;				
			}
		}

		if($wrapper && !empty($ret)){
			return '<div class="gdlr-session-info">' . $ret . '<div class="clear"></div></div>';
		}else if( !empty($ret) ){
			return $ret . '<div class="clear"></div>';
		}
		return '';
	}	

	// add action to check for speaker item
	add_action('gdlr_print_item_selector', 'gdlr_check_session_item', 10, 2);
	function gdlr_check_session_item( $type, $settings = array() ){
		if($type == 'session'){
			gdlr_print_session_item( $settings );
		}else if($type == 'session-counter'){
			gdlr_print_session_counter_item( $settings );
		}
	}

	// print session counter item
	function gdlr_print_session_counter_item( $settings ){
		$item_id = empty($settings['page-item-id'])? '': ' id="' . $settings['page-item-id'] . '" ';

		global $gdlr_spaces;
		$margin = (!empty($settings['margin-bottom']) && 
			$settings['margin-bottom'] != $gdlr_spaces['bottom-item'])? 'margin-bottom: ' . $settings['margin-bottom'] . ';': '';
		$margin_style = (!empty($margin))? ' style="' . $margin . '" ': '';
		
		$session_post = get_posts( array('post_type'=>'session', 'name'=>$settings['select-session'], 'posts_per_page'=>1) );
		$session_time = strtotime(get_post_meta($session_post[0]->ID, 'session-date', true));	
		$current_date = strtotime(current_time('mysql'));
		
		
		if( $session_time > $current_date ){
			$total_time = $session_time - $current_date;
			$day = intval($total_time / 86400);
			
			$total_time = $total_time % 86400;
			$hrs = intval($total_time / 3600);
			
			$total_time = $total_time % 3600;
			$min = intval($total_time / 60);
			$sec = $total_time % 60;
			
			echo '<div class="gdlr-session-counter-item gdlr-item" ' . $item_id . $margin_style . ' >';
			echo '<div class="session-counter-block gdlr-block-day" >';
			echo '<span class="gdlr-time gdlr-day">' . $day . '</span>';
			echo '<span class="gdlr-unit">' . __('Days', 'gdlr-conference') . '</span>';
			echo '</div>';
			
			echo '<div class="session-counter-block gdlr-block-hrs" >';
			echo '<span class="gdlr-time gdlr-hrs">' . $hrs . '</span>';
			echo '<span class="gdlr-unit">' . __('Hours', 'gdlr-conference') . '</span>';
			echo '</div>';

			echo '<div class="session-counter-block gdlr-block-min" >';
			echo '<span class="gdlr-time gdlr-min">' . $min . '</span>';
			echo '<span class="gdlr-unit">' . __('Mins', 'gdlr-conference') . '</span>';
			echo '</div>';

			echo '<div class="session-counter-block gdlr-block-sec" >';
			echo '<span class="gdlr-time gdlr-sec">' . $sec . '</span>';
			echo '<span class="gdlr-unit">' . __('Secs', 'gdlr-conference') . '</span>';
			echo '</div>';
			echo '<div class="clear"></div>';
 			echo '</div>';			
		}
	}
	
	// print session counter item
	function gdlr_print_session_item( $settings ){
		$item_id = empty($settings['page-item-id'])? '': ' id="' . $settings['page-item-id'] . '" ';

		global $gdlr_spaces;
		$margin = (!empty($settings['margin-bottom']) && 
			$settings['margin-bottom'] != $gdlr_spaces['bottom-item'])? 'margin-bottom: ' . $settings['margin-bottom'] . ';': '';
		$margin_style = (!empty($margin))? ' style="' . $margin . '" ': '';

		
		
		$args = array(
		    'post_type'  => 'session',			   
		    'posts_per_page' => 9999,
		    'meta_key' => 'session-date',
		    'orderby' => 'session-date',
		    'order' => 'asc'
		);
		

		if( !empty($settings['category']) ){ 
			$args['tax_query'] = array(
				array('terms'=>explode(',', $settings['category']), 'taxonomy'=>'session_category', 'field'=>'slug')
			);	
		}
		$query = new WP_Query( $args );		
		
				

		$swapped = false;
		for($loop=0;$loop<count($query->posts)-1;$loop++) {			
			for($i=$loop;$i<count($query->posts);$i=$i+2) {				
					$a = json_decode(get_post_meta( $query->posts[$i]->ID, 'post-option', true ));
					$b = json_decode(get_post_meta( $query->posts[$i+1]->ID, 'post-option', true ));
					$a_time = str_replace(' ','',$a->{'session-time'});
					$b_time = str_replace(' ','',$b->{'session-time'});
					if(($a->{'session-date'} == '2015-06-05' || $b->{'session-date'} == '2015-06-05') && $swapped == false && strcmp($a_time,$b_time)==0) {
						$a = json_decode(get_post_meta( $query->posts[$i+1]->ID, 'post-option', true ));
						$b = json_decode(get_post_meta( $query->posts[$i+2]->ID, 'post-option', true ));
						$swapped = true;						
					}
					
					if($a->{'session-date'} == $b->{'session-date'} && $a_time == $b_time && strcmp($a->location, $b->location) > 0) {
						$tmp = $query->posts[$i];
						$query->posts[$i] = $query->posts[$i+1];
						$query->posts[$i+1] = $tmp;
					}				
			}
		}		
			
		
		
		// set the excerpt length
		if( !empty($settings['num-excerpt']) ){
			global $gdlr_excerpt_length; $gdlr_excerpt_length = $settings['num-excerpt'];
			add_filter('excerpt_length', 'gdlr_set_excerpt_length');
		} 		
		
		echo gdlr_get_item_title($settings);
		// dev - add grid list view icon
		$view = '<div class="view-type"><label>Viewing Options: </label><a href="/program" id="get-list-view"><i class="fa fa-list"></i></a><a href="/program/?type=grid" id="get-grid-view"><i class="fa fa-th"></i></a></div>';
		echo $view;
		echo '<div class="session-item-wrapper" ' . $item_id . $margin_style . ' >'; 	
		$type = $_GET['type'];
		if($type != 'grid'){
			if( $settings['session-style'] == 'full' ){
				gdlr_print_full_session($query);
			}else if( $settings['session-style'] == 'tab' ){
				gdlr_print_tab_session($query);
			}else if( $settings['session-style'] == 'small' ){
				gdlr_print_small_session($query);
			}
		}
		else { ?>
			<div class="gdlr-item-title-head">
				<h3 class="fix-title gdlr-item-title gdlr-skin-title gdlr-skin-border gdlr-title-large">IT4K12 2015 - Program at a Glance</h3>
			</div>
		<?php
			gdlr_grid_session($query);	
		}
		
		
		remove_filter('excerpt_length', 'gdlr_set_excerpt_length');
		
		echo '<div class="clear"></div>';
		echo '</div>'; // speaker item wrapper
	}	
	
	// print full session item
	function gdlr_print_full_session($query){
		global $theme_option;
		if( !empty($theme_option['new-fontawesome']) && $theme_option['new-fontawesome'] == 'enable' ){
			$icon_class = array('time'=>'fa-clock-o');
		}else{
			$icon_class = array('time'=>'icon-time');
		}	
		
		$current_session_day = 0;
		$current_session_date = '';
		
		echo '<div class="gdlr-session-item gdlr-full-session-item gdlr-item" >';
		while($query->have_posts()){ $query->the_post();
			$gdlr_post_option = gdlr_decode_preventslashes(get_post_meta(get_the_ID(), 'post-option', true));
			$gdlr_post_option = json_decode($gdlr_post_option, true);
			$gdlr_speakers = gdlr_get_session_speaker_list($gdlr_post_option['session-speaker']);
			
			$session_date_o = strtotime(get_post_meta(get_the_ID(), 'session-date', true));
			$session_date = date_i18n($theme_option['date-format'], $session_date_o);
			
			if( $current_session_date != $session_date ){
				$current_session_day++;
				$current_session_date = $session_date;
				
				echo '<div class="gdlr-session-item-head ' . (($current_session_day == 1)? 'gdlr-first': '') . '">';
				echo '<div class="gdlr-session-item-head-info gdlr-active">';
				echo '<div class="gdlr-session-head-day">' . sprintf(__('Day %d', 'gdlr-conference'), $current_session_day) . '</div>';
				echo '<div class="gdlr-session-head-date">' . $current_session_date . '</div>';
				echo '</div>';
				echo '</div>';
			}
			
			echo '<div class="gdlr-session-item-content-wrapper">';
			echo '<div class="gdlr-session-item-divider"></div>';
			if( !empty($gdlr_post_option['session-type']) && $gdlr_post_option['session-type'] == 'break' ){
				
				// techtone: start add location for break type
				echo '<div class="gdlr-session-item-content-info" style="margin-top:10px">';
				echo gdlr_get_session_info(array('location'), $gdlr_post_option); 
				echo '</div>';
				// techtone: end add location for break type
				
				echo '<div class="session-break-content">';
				echo '<div class="session-break-info">';
				echo '<i class="fa ' . $icon_class['time'] . '" ></i>';
				echo $gdlr_post_option['session-time'];						
				echo '</div>';	
				echo '<h3 class="gdlr-session-break-title">' . get_the_title() . '</h3>'; 
				echo '</div>';
			}else{			
				echo '<div class="gdlr-session-item-content-info">';
				echo gdlr_get_session_info(array('time', 'location', 'speaker'), $gdlr_post_option, $gdlr_speakers); 
				echo '</div>';
				
				echo '<div class="gdlr-session-item-content" >';
				echo '<h3 class="gdlr-session-item-title"><a href="' . get_permalink() . '">' . get_the_title() . '</a></h3>'; 
				echo '<div class="gdlr-session-item-readmore">' . get_the_content() . '</div>';
				
				if( !empty($gdlr_speakers) ){
					echo '<div class="gdlr-session-thumbnail-wrapper">';
					foreach( $gdlr_speakers as $speaker ){
						echo gdlr_get_speaker_thumbnail('program-thumb', $speaker->ID, array(), true, true);
					}
					echo '</div>';

					//sondoha: add present text

					echo '<div class="gdlr-session-speaker-name-wrapper">';
					
					//var_dump($gdlr_speakers);
					$_speakers = array();
					foreach( $gdlr_speakers as $speaker ){
						if(is_object($speaker)) {
							$_speakers[] = gdlr_get_speaker_name($speaker->ID);
						}
					}
					if(count($_speakers)) {
						echo '<span class="speaker-text-label">' . __('Presented By', 'gdlr-conference') . ': </span>';
						echo implode(", ",$_speakers);
					}
					echo '</div>';
					echo '<div style="clear:both;height:0">&nbsp;</div>';
				}
				
				echo '</div>';
			}
			echo '<div class="clear"></div>';
			echo '</div>'; // session-item-content-wrapper
		}
		echo '</div>'; // gdlr-full-session-item
		
		wp_reset_postdata();
		
	}
	
	// print tab session item
	function gdlr_print_tab_session($query){
		global $theme_option;
		if( !empty($theme_option['new-fontawesome']) && $theme_option['new-fontawesome'] == 'enable' ){
			$icon_class = array('time'=>'fa-clock-o');
		}else{
			$icon_class = array('time'=>'icon-time');
		}	
		
		echo '<div class="gdlr-session-item gdlr-tab-session-item gdlr-item" >';
		echo '<div class="gdlr-session-item-head"  >';
		
		$current_session_day = 0;
		$current_session_date = '';
		while($query->have_posts()){ $query->the_post();
			$session_date_o = strtotime(get_post_meta(get_the_ID(), 'session-date', true));
			$session_date = date_i18n($theme_option['date-format'], $session_date_o);

			if( $current_session_date != $session_date ){
				$current_session_day++;
				$current_session_date = $session_date;

				echo '<div class="gdlr-session-item-head-info ' . (($current_session_day == 1)? 'gdlr-active': '') . '" data-tab="gdlr-tab-' . $current_session_day . '">';
				echo '<div class="gdlr-session-head-day">' . sprintf(__('Day %d', 'gdlr-conference'), $current_session_day) . '</div>';
				echo '<div class="gdlr-session-head-date">' . $current_session_date . '</div>';
				echo '</div>';
			}
		}
		echo '<div class="clear"></div>';
		echo '</div>'; // session-item-head
		rewind_posts();
		
		$current_session_day = 0;
		$current_session_date = '';
		while($query->have_posts()){ $query->the_post();
			$gdlr_post_option = gdlr_decode_preventslashes(get_post_meta(get_the_ID(), 'post-option', true));
			$gdlr_post_option = json_decode($gdlr_post_option, true);
			$gdlr_speakers = gdlr_get_session_speaker_list($gdlr_post_option['session-speaker']);		

			$session_date_o = strtotime(get_post_meta(get_the_ID(), 'session-date', true));
			$session_date = date_i18n($theme_option['date-format'], $session_date_o);
		
			if( $current_session_date != $session_date ){
				$current_session_day++;
				$current_session_date = $session_date;
				
				echo ($current_session_day == 1)? '': '</div>'; // gdlr-session-item-tab-content 
				echo '<div class="gdlr-session-item-tab-content gdlr-tab-' . $current_session_day . ' ' . (($current_session_day == 1)? 'gdlr-active': '') . '">';
			}		
		
			echo '<div class="gdlr-session-item-content-wrapper">';
			echo '<div class="gdlr-session-item-divider"></div>';
			if( !empty($gdlr_post_option['session-type']) && $gdlr_post_option['session-type'] == 'break' ){
				echo '<div class="session-break-content">';
				echo '<div class="session-break-info">';
				echo '<i class="fa ' . $icon_class['time'] . '" ></i>';
				echo $gdlr_post_option['session-time'];						
				echo '</div>';	
				echo '<h3 class="gdlr-session-break-title">' . get_the_title() . '</h3>'; 
				echo '</div>';
			}else{
				echo '<div class="gdlr-session-item-content-info">';
				echo gdlr_get_session_info(array('time', 'location', 'speaker'), $gdlr_post_option, $gdlr_speakers); 
				echo '</div>';
				
				echo '<div class="gdlr-session-item-content" >';
				echo '<h3 class="gdlr-session-item-title"><a href="' . get_permalink() . '">' . get_the_title() . '</a></h3>'; 
				echo '<div class="gdlr-session-item-excerpt">' . get_the_excerpt() . '</div>';
				
				if( !empty($gdlr_speakers) ){
					echo '<div class="gdlr-session-thumbnail-wrapper">';
					foreach( $gdlr_speakers as $speaker ){
						echo gdlr_get_speaker_thumbnail('thumbnail', $speaker->ID, array(), true, true);
					}
					echo '</div>';
				}
				
				echo '</div>';
			}
			echo '<div class="clear"></div>';
			echo '</div>'; // session-item-content-wrapper
			
		}
		echo '</div>'; // gdlr-session-item-tab-content 
		echo '</div>'; // gdlr-tab-session-item
		
		wp_reset_postdata();
		
	}

	// print small session item
	function gdlr_print_small_session($query){
		global $theme_option;
		if( !empty($theme_option['new-fontawesome']) && $theme_option['new-fontawesome'] == 'enable' ){
			$icon_class = array('time'=>'fa-clock-o');
		}else{
			$icon_class = array('time'=>'icon-time');
		}	
		
		echo '<div class="gdlr-session-item gdlr-small-session-item gdlr-item" >';
		echo '<div class="gdlr-session-item-head" >';
		
		$current_session_day = 0;
		$current_session_date = '';
		while($query->have_posts()){ $query->the_post();
			$session_date_o = strtotime(get_post_meta(get_the_ID(), 'session-date', true));
			$session_date = date_i18n($theme_option['date-format'], $session_date_o);

			if( $current_session_date != $session_date ){
				$current_session_day++;
				$current_session_date = $session_date;

				echo '<div class="gdlr-session-item-head-info ' . (($current_session_day == 1)? 'gdlr-active': '') . '" data-tab="gdlr-tab-' . $current_session_day . '">';
				echo '<div class="gdlr-session-head-day">' . sprintf(__('Day %d', 'gdlr-conference'), $current_session_day) . '</div>';
				echo '<div class="gdlr-session-head-date">' . $current_session_date . '</div>';
				echo '</div>';
			}
		}
		echo '<div class="clear"></div>';
		echo '</div>'; // session-item-head
		rewind_posts();
		
		$current_session_day = 0;
		$current_session_date = '';
		while($query->have_posts()){ $query->the_post();
			$gdlr_post_option = gdlr_decode_preventslashes(get_post_meta(get_the_ID(), 'post-option', true));
			$gdlr_post_option = json_decode($gdlr_post_option, true);
			$gdlr_speakers = gdlr_get_session_speaker_list($gdlr_post_option['session-speaker']);		

			$session_date_o = strtotime(get_post_meta(get_the_ID(), 'session-date', true));
			$session_date = date_i18n($theme_option['date-format'], $session_date_o);
		
			if( $current_session_date != $session_date ){
				$current_session_day++;
				$current_session_date = $session_date;
				
				echo ($current_session_day == 1)? '': '</div>'; // gdlr-session-item-tab-content 
				echo '<div class="gdlr-session-item-tab-content gdlr-tab-' . $current_session_day . ' ' . (($current_session_day == 1)? 'gdlr-active': '') . '">';
			}		
		
			echo '<div class="gdlr-session-item-content-wrapper">';
			echo '<div class="gdlr-session-item-divider"></div>';
			
			if( !empty($gdlr_post_option['session-type']) && $gdlr_post_option['session-type'] == 'break' ){
				echo '<div class="session-break-content">';
				echo '<div class="session-break-info">';
				echo '<i class="fa ' . $icon_class['time'] . '" ></i>';
				echo $gdlr_post_option['session-time'];						
				echo '</div>';	
				echo '<h3 class="gdlr-session-break-title">' . get_the_title() . '</h3>'; 
				echo '</div>';
			}else{
				echo '<div class="gdlr-session-item-content" >';
				if( !empty($gdlr_speakers) ){
					echo '<div class="gdlr-session-thumbnail-wrapper">';
					echo gdlr_get_speaker_thumbnail('thumbnail', $gdlr_speakers[0]->ID, array(), true, true);
					echo '</div>';
				}			
				
				echo '<div class="gdlr-session-item-content-inner" >';
				echo '<h3 class="gdlr-session-item-title"><a href="' . get_permalink() . '">' . get_the_title() . '</a></h3>'; 
				echo '<div class="gdlr-session-item-content-info">';
				echo gdlr_get_session_info(array('time', 'location', 'speaker'), $gdlr_post_option, $gdlr_speakers); 
				echo '</div>'; // session-item-content-info		 
				echo '</div>'; // session-item-content-inner
				echo '</div>'; // session-item-content
			}
			
			echo '<div class="clear"></div>';
			echo '</div>'; // session-item-content-wrapper
			
		}
		echo '</div>'; // gdlr-session-item-tab-content 
		echo '</div>'; // gdlr-tab-session-item
		
		wp_reset_postdata();
		
	}	

	/**
	dev - add grid session display
	*/
	// dev - get list date.
	function getListDateSession(){

		$time = array();

		global $wpdb;

		$listDate = $wpdb->get_results("
			SELECT pm.meta_value as date		       
			FROM $wpdb->posts p
			JOIN $wpdb->postmeta pm on p.ID = pm.post_id
			WHERE p.post_type = 'session'
			AND p.post_status = 'publish'
			AND pm.meta_key = 'session-date'
			GROUP BY pm.meta_value
			ORDER BY pm.meta_value ASC"
		);

		foreach($listDate as $date){
			$t = explode(' ', $date->date);
			$time[] = $t[0];
		}

		$dateData = array_unique($time);

		return $dateData;

	}

	function getListTimeBySessionDate($query){
		$listTime = array();
		while($query->have_posts()){ $query->the_post();
					global $post;				
					$session_option = get_post_meta($post->ID, 'post-option', true);
					$options = json_decode($session_option, true);
					$listTime[] = str_replace(' ', '', $options['session-time']);
		}
		$timeData = array_unique($listTime);
		$timeData = array_values($timeData);			
		return $timeData;
	}

	function getSessionByTime($time, $date){
		$time_args = array(
				    'post_type'  => 'session',			   
				    'posts_per_page' => 10,
				    'meta_key' => 'order_session',				    
				    'orderby' => 'order_session',
				    'order' => 'asc',
				    'meta_query' => array(
				    	'relation' => 'AND',
				        array(
				            'key' => 'session-date',
				            'value' => $date,
				            'compare' => 'LIKE'
				        ),
						array(
							'key'     => 'session_time',
							'value'   => $time,
							'compare' => '=',
						),
					),
				);
		$time_query = new WP_Query( $time_args );
		return $time_query;
	}

	function gdlr_grid_session(){

		$listDate = getListDateSession();
		ob_start();
		echo '<div class="grid-session">';
		$mobileData = array();				
		foreach($listDate as $date):
			$current_date_timestamp = strtotime($date);
			$date_to_display = date('l, F d', $current_date_timestamp);
		?>			
			<h3 class="gr-date"><?php echo $date_to_display; ?> <span class="wifi-info">WIFI login: IT4K12 + Twitter Hashtag: #IT4K12</span></h3>
			<?php
				//get list session by current date
				$args = array(
				    'post_type'  => 'session',			   
				    'posts_per_page' => 9999,
				    'meta_key' => 'session-date',				    
				    'orderby' => 'session-date',
				    'order' => 'ASC',
				    'meta_query' => array(
						array(
							'key'     => 'session-date',
							'value'   => $date,
							'compare' => 'LIKE',
						),
					),
				);
				$query = new WP_Query( $args );
				$list_time = getListTimeBySessionDate($query);
				echo '<div class="block-session">';
				$k=1;			
				$mobileData[$date] = array();	
				$sessionBreakCount = 0;
				for($i=0; $i<count($list_time); $i++){

					/*global $post;
					$session_option = get_post_meta($post->ID, 'post-option', true);
					$options = json_decode($session_option, true);	
													*/						
				//	var_dump($list_time);
					if($list_time[$i]!=''):						
						$item = getSessionByTime($list_time[$i], $date);
						$count =  $item->post_count;
						$width = 100 / $count;	
						

								
					?>
						<div class="item-date-session <?php if($count==1){echo 'single-session';}?>">
							<div class="item-time">
								<i class="fa fa-clock-o"></i><?php echo str_replace('-', ' - ', $list_time[$i]); ?>
							</div>
							<div class="item-information">								
								
								<?php
									$subItemCount = 0;
									while($item->have_posts()){ $item->the_post(); $subItemCount++;
										global $post;
										$session_option = get_post_meta($post->ID, 'post-option', true);
										$options = json_decode($session_option, true);
										$gdlr_speakers = gdlr_get_session_speaker_list($options['session-speaker']);
										//var_dump($gdlr_speakers);		
										$mobileData[$date][] = array(
											'date' => $date_to_display,
											'title'=> get_the_title($post->ID),
											'time' => str_replace('-', ' - ', $list_time[$i]),
											'link' => get_permalink($post->ID),
											'speaker'=>$gdlr_speakers
										);
										if($options['session-type'] == 'break'){
											?>
											<div class="item-name"><?php echo get_the_title($post->ID); ?></div>											
											<div class="item-location"><?php echo gdlr_get_session_info(array('location'), $options); ?></div>
											<?php
										} else {	
											//$item_term = get_the_terms( $post->ID, 'track_manage' );
											$term_id = $options['session-track'];
											$term = get_term( $term_id, 'track_manage' );		

											if($term_id){
												//$term = array_pop($item_term);																								
												$color = get_field('track_color', $term);												
											}
											else {
												$color='none';
											}										
											if($subItemCount == 1 and $color != 'none' ): $sessionBreakCount++;?>
												<div class="session-break-title">SESSION <?php echo $sessionBreakCount; ?> BREAKOUT</div>
											<?php endif; 																				
											$content = apply_filters('the_content', get_post_field('post_content', $post->ID));

											if(strlen($content)>0){
												$_content = substr($content, 0, 100);
											}
											else {
												$_content='';
											}
											
											?>
											<!--div class="break-title">SESSION <?php echo $k; ?> BREAKOUT</div-->
											<?php //if($count>1): ?>
											<div class="item-block" style="width:<?php echo $width?>%">
												<div class="item-header" style="background:<?php echo $color?>">
													<div class="header-item-name">
														<a href="<?php echo get_permalink($post->ID)?>"><?php echo $term->name?></a>
													</div>
													<span><?php echo gdlr_get_session_info(array('location'), $options); ?></span>
												</div>
												<div class="item-exceprt">
													<a href="<?php echo get_permalink($post->ID)?>"><?php echo get_the_title($post->ID)?> </a>
													<?php 
														$_speakers = array();
														foreach( $gdlr_speakers as $speaker ){
															if(is_object($speaker)) {
																$_speakers[] = gdlr_get_speaker_name($speaker->ID);
															}
														}
													?>																					
													<span class="item-speaker"><?php echo implode(", ",$_speakers);?></span>

												</div>
											</div>
										<?php /*else: ?>
											<div class="single-session" style="background:<?php echo $color?>">
												<div class="item-name"><?php echo get_the_title($post->ID); ?></div>											
												<div class="item-location"><?php echo gdlr_get_session_info(array('location','no-track'), $options); ?></div>	
												<?php 
															$_speakers = array();
															foreach( $gdlr_speakers as $speaker ){
																if(is_object($speaker)) {
																	$_speakers[] = gdlr_get_speaker_name($speaker->ID);
																}
															}
													?>																					
													<span class="item-speaker"><?php echo implode(", ",$_speakers);?></span>	
											</div>									
										<?php endif; */ 
											
										}
									}//endwhile
								?>
							</div>
						</div>
					<?php
					endif;
				}//end for			
				echo '</div>';	
			 ?>
		<?php		
		endforeach;
		echo '</div>';
		/*Mobile list*/					
			?>
				<div class="mobile-list">
					<div class="mobile-items">
						<ul class="m-block-lists">
				<?php foreach($listDate as $date):?>
							<li class="m-list-item">
								<div class="m-item-date">
								<?php
									 echo $mobileData[$date][0]['date'];
								 ?>
								</div>
								<ul class="m-block-list-item">
									<?php for($i=0; $i<count($mobileData[$date]); $i++ ):?>
										<li class="m-block-item">
											<a href="<?php echo $mobileData[$date][$i]['link']?>" title="" alt="">
												<div class="m-item-title"><?php echo $mobileData[$date][$i]['title']; ?></div>
												<div class="m-item-time"><?php echo $mobileData[$date][$i]['time']?></div>
											</a>
											<div class="m-desc-information">
												<?php 													
													$_speakers = array();
														foreach( $mobileData[$date][$i]['speaker'] as $speaker ){
															if(is_object($speaker)) {
																$_speakers[] = gdlr_get_speaker_name($speaker->ID);
															}
														}
												?>
												<?php if(count($_speakers)>1): ?>
													<span class="m-item-speaker">Speaker: <?php echo implode(", ",$_speakers);?></span>
												<?php endif;?>
											</div>
										</li>
									<?php endfor; ?>
								</ul>
							</li>

				<?php endforeach; ?>
						</ul>
					</div>
				</div>
			<?php
		/*end mobile list*/
		?>

			<script type="text/javascript">
				/*set index for session breakout title*/
				jQuery(document).ready(function($){
					$('.session-break-title').each(function(index){						
						i = index + 1;
						$(this).find('.title-index').html(i);
					})
				})
			</script>
		<?php

		$output = ob_get_clean();

		echo $output;
	}

	//add_action('wp_footer', 'save_time_session');
	function save_time_session(){
		$args = array(
		    'post_type'  => 'session',			   
		    'posts_per_page' => 9999,
		    'meta_key' => 'session-date',
		    'orderby' => 'session-date',
		    'order' => 'asc'
		);

		$query = new WP_Query( $args );
		while($query->have_posts()){ $query->the_post();
					global $post;				
					/*$session_option = get_post_meta($post->ID, 'post-option', true);
					$options = json_decode($session_option, true);
					$listTime = str_replace(' ', '', $options['session-time']);
					add_post_meta($post->ID, 'session_time', $listTime);*/
					$order = get_post_meta($post->ID, 'order_session', true);
					if($order=='' || $oder == NULL ){
						add_post_meta($post->ID, 'order_session', 1);
					}
		}
	}
?>