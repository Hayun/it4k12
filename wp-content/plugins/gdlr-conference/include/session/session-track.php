<?php
	/*	
	*	Techtone Session Track File
	*/	

class Techtone_Session_Track {

	private $_data = array();

	private $_trackId;

	public function __construct() {		
		$tracks = get_terms( 'track_manage', array('hide_empty' => false) );	
		$this->_data[] = array(null, __('Select Session Track', 'gdlr-conference'));	 		
		foreach($tracks as $track) {
			$color = get_field('track_color',$track);			
			$this->_data[$track->term_id] = array($color,$track->name);
		}
		return $options;
		/*
		$this->_data = array(
		array(null, __('Select Session Track', 'gdlr-conference')),		
		array('#948fb3', __('Creating Capacity Through District Collaboration', 'gdlr-conference')),
		array('#e35952', __('Nuts & Bolts', 'gdlr-conference')),
		array('#edd91f', __('Customer Experience & Service Quality', 'gdlr-conference')),
		array('#f79a20', __('BC Digital Classroom: Best Practices, Tips & Tools', 'gdlr-conference')),
		array('#1ca4c9', __('Elevating Technology in the Classroom', 'gdlr-conference')),
		array('#739c6b', __('Creating Connections', 'gdlr-conference')),
		//array('#77d0d1', __('Managing Work & the Workforce', 'gdlr-conference')),
		//array('#474545', __('Educators Elevating Learning In the Classroom', 'gdlr-conference'))
		);
		*/
	}

	public function setTrackId($id) {
		$this->_trackId = $id;
	}

	public function getDataArray() {
		$options = array();
		foreach($this->_data as $index => $option) {
			$options[$index] = $option[1];
		}
		return $options;
	}

	public function getTrackValue() {		
		if(!isset($this->_data[$this->_trackId]) || $this->_trackId == 0) return false;
		return $this->_data[$this->_trackId][1];		
	}

	public function getTrackColor() {
		//return '#1ca4c9';
		if(!isset($this->_data[$this->_trackId]) || $this->_trackId == 0) return false;
		return $this->_data[$this->_trackId][0];
	}

}

?>