<?php
require_once './database.php';

/**
 * Checks a user's login information and logs them in if it checks out.
 *
 * @since 2.5.0
 *
 * @param string $username User's username
 * @param string $password User's password
 * @return WP_User|WP_Error WP_User object if login successful, otherwise WP_Error object.
 */
function wp_authenticate($username, $password) {
	$username = sanitize_user($username);
	$password = trim($password);
	$db = new SDatabase();
	
	/**
	 * Filter the user to authenticate.
	 *
	 * If a non-null value is passed, the filter will effectively short-circuit
	 * authentication, returning an error instead.
	 *
	 * @since 2.8.0
	 *
	 * @param null|WP_User $user     User to authenticate.
	 * @param string       $username User login.
	 * @param string       $password User password
	 */
	$user = apply_filters( 'authenticate', null, $username, $password );

	if ( $user == null ) {
		echo 'test';
		// TODO what should the error message be? (Or would these even happen?)
		// Only needed if all authentication handlers fail to return anything.
		$user = new WP_Error('authentication_failed', __('<strong>ERROR</strong>: Invalid username or incorrect password.'));
	}

	$ignore_codes = array('empty_username', 'empty_password');

	if (is_wp_error($user) && !in_array($user->get_error_code(), $ignore_codes) ) {
		echo 'test 2';
		/**
		 * Fires after a user login has failed.
		 *
		 * @since 2.5.0
		 *
		 * @param string $username User login.
		 */
		do_action( 'wp_login_failed', $username );
	}

	return $user;
}

/**
 * Retrieve user info by a given field
 *
 * @since 2.8.0
 *
 * @param string $field The field to retrieve the user with. id | slug | email | login
 * @param int|string $value A value for $field. A user ID, slug, email address, or login name.
 * @return WP_User|bool WP_User object on success, false on failure.
 */
function get_user_by( $field, $value ) {
	$userdata = WP_User::get_data_by( $field, $value );

	if ( !$userdata )
		return false;

	$user = new WP_User;
	$user->init( $userdata );

	return $user;
}