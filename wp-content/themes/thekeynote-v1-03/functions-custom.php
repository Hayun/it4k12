<?php

/**

  *	Create shortcode to show Attendees list

  */

add_shortcode('attendee_list', 'attendee_list_action');



function attendee_list_action(){

	ob_start();

	$args = array( 
		'post_type' => 'attendee', 
		'orderby'=>'title', 
		'order'=>'ASC' , 
		'posts_per_page' => -1 
		);

	$loop = new WP_Query( $args );

	?>

	<div class="speaker-item-wrapper attendee_list" style="margin-bottom: 40px;"><div class="speaker-item-holder gdlr-speaker-type-round"><div class="clear"></div>

	<?php

	$i = 0;

	while ( $loop->have_posts() ) : $loop->the_post();

	global $post;

	// echo '<pre>';var_dump(get_the_excerpt());exit;

	$excerpt = get_the_excerpt();

	$district_name_a = get_post_meta($post->ID, '', true); 
	$district_name = $district_name_a['wpcf-district-name-select'][0];

	$facebook_url = get_post_meta($post->ID, '', true);
	$facebook_url = $facebook_url['wpcf-facebook-url'][0];

	$twitter_url = get_post_meta($post->ID, '', true);
	$twitter_url = $twitter_url['wpcf-twitter-url'][0];

	$google_url = get_post_meta($post->ID, '', true);
	$google_url = $google_url['wpcf-google-plus'][0];

	$linkedin_url = get_post_meta($post->ID, '', true);
	$linkedin_url = $linkedin_url['wpcf-linkedin-url'][0];

	$blog_url = get_post_meta($post->ID, '', true);
	$blog_url = $blog_url['wpcf-blog-url'][0];

	$bio_content = get_post_meta($post->ID, '', true);
	$bio_content = $bio_content['except'][0];

	$attendee_avatar = get_the_post_thumbnail($post->ID, array(150, 150));
	

	$post_count = wp_count_posts( 'attendee', '')->publish;

	$i++;

	if($i%6 == 0){

		$stt_condition = 1;

	}

	else{

		$stt_condition = 0;

	}

?>

	<div class="two columns">

		<div class="gdlr-item gdlr-speaker-item attendee_item">

			<div class="gdlr-ux gdlr-speaker-item-ux" style="opacity: 1; padding-top: 0px; margin-bottom: 0px;">

				<div class="gdlr-speaker-thumbnail">

					<?php 

						if(has_post_thumbnail()){

							echo $attendee_avatar;

						}

						else{

							echo '<img alt="avatar" src="'.get_template_directory_uri().'/images/default-avatar.jpg" />';

						}

					?>

					

				</div>

				<div class="gdlr-speaker-item-content attendee_item_content">

					<h3 class="gdlr-speaker-item-title gdlr-skin-title"><span class="attendee_name"><?php echo the_title()?></span></h3>

					<div class="gdlr-speaker-item-position gdlr-info-font gdlr-skin-info district_field"><?php echo $district_name; ?></div>

					<ul class="attendee_socials">

						<?php if(!empty($facebook_url)): ?>

							<li><a target="_blank" href="<?php echo $facebook_url; ?>"><i class="fa fa-facebook"></i></a></li>

						<?php endif ?>

						<?php if(!empty($twitter_url)): ?>

							<li><a target="_blank" href="<?php echo $twitter_url; ?>"><i class="fa fa-twitter"></i></a></li>

						<?php endif ?>

						<?php if(!empty($google_url)): ?>

							<li><a target="_blank" href="<?php echo $google_url; ?>"><i class="fa fa-google-plus"></i></a></li>

						<?php endif ?>

						<?php if(!empty($linkedin_url)): ?>

							<li><a target="_blank" href="<?php echo $linkedin_url; ?>"><i class="fa fa-linkedin"></i></a></li>

						<?php endif ?>

						<?php if(!empty($blog_url)): ?>

							<li><a target="_blank" href="<?php echo $blog_url; ?>"><i class="fa fa-home"></i></a></li>

						<?php endif ?>						

					</ul>

					<div class="bio_field"><?php if(!empty($excerpt)): echo do_shortcode('[simple_tooltip content="'.$excerpt.'"]Bio[/simple_tooltip]'); endif; ?></div>

				</div>

			</div>

		</div>

	</div>	

<?php

	if ($stt_condition == 1) {

		echo '<div class="clear"></div>';

	}

	endwhile;

	wp_reset_query();

	?>

	</div></div>

	<?php

	echo ob_get_clean();

}





/**

 * MSSQL Database config

 */

function get_mssql_connection() {	

	if(!class_exists('SDatabase')) {

	require_once dirname( __FILE__ ) . "/include/database.php";

	}

	if(!isset($GLOBALS['mssqldb'])) {

		$GLOBALS['mssqldb'] = new SDatabase('191.239.4.7', 'sa', 'Erac2011', 'EIS_RELEASE_COMBINE');

	}

	return $GLOBALS['mssqldb'];

}



/**

 * Conference Room ID

 */

function get_the_conference_room_ID() {

	return 4;

}





function _anh_debug($var)

{

    echo "<pre>";

    var_dump($var);

    echo "</pre>";

}



function anh_save_post_speaker($post_id, $post, $update) {

    if ($post->post_type !== 'speaker') {

        return $data;

    }



    $speakerOption = get_post_meta($post_id, 'post-option');

    $speakerOption = json_decode($speakerOption[0], true);



    $speakerSocial = array();

    $socialTypes = array('facebook','twitter','linkedin','google');

    for ($i=1; $i <= 4; $i++) { 

        $iconName = 'icon-'.$i;

        $iconLink = 'icon-link-'.$i;



        

        $name = $socialTypes[$i-1];

        $link = $speakerOption[$iconLink];



        if(!empty($link))

        $speakerSocial[] = "[gdlr_social type=\"{$name}\"]{$link}[/gdlr_social]";

        

    }



    $speakerSocial = implode($speakerSocial, "|g1n|");

    $speakerOption['speaker-social'] = str_replace('"', "|gq2|", $speakerSocial);



    $speakerOption = gdlr_preventslashes(json_encode($speakerOption));

    update_post_meta($post_id, 'post-option', $speakerOption);

}



add_filter( 'save_post' , 'anh_save_post_speaker' , 10, 3 );

