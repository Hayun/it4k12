	<?php global $theme_option; ?>
	<div class="clear" ></div>
	</div><!-- content wrapper -->

	<?php 
		// page style
		global $gdlr_post_option;
		if( empty($gdlr_post_option) || empty($gdlr_post_option['page-style']) ||
			  $gdlr_post_option['page-style'] == 'normal' || 
			  $gdlr_post_option['page-style'] == 'no-header'){ 
	?>	
	<footer class="footer-wrapper" >
		<?php if( $theme_option['show-footer'] != 'disable' ){ ?>
		<div class="footer-container container">
			<?php 	
				$i = 1;
				$theme_option['footer-layout'] = empty($theme_option['footer-layout'])? '1': $theme_option['footer-layout'];
				$gdlr_footer_layout = array(
					'1'=>array('twelve columns'),
					'2'=>array('three columns', 'three columns', 'three columns', 'three columns'),
					'3'=>array('three columns', 'three columns', 'six columns',),
					'4'=>array('four columns', 'four columns', 'four columns'),
					'5'=>array('four columns', 'four columns', 'eight columns'),
					'6'=>array('eight columns', 'four columns', 'four columns'),
				);
			?>
			<?php foreach( $gdlr_footer_layout[$theme_option['footer-layout']] as $footer_class ){ ?>
				<div class="footer-column <?php echo esc_attr($footer_class); ?>" id="footer-widget-<?php echo esc_attr($i); ?>" >
					<?php dynamic_sidebar('Footer ' . $i); ?>
				</div>
			<?php $i++; ?>
			<?php } ?>
			<div class="clear"></div>
		</div>
		<?php } ?>
		
		<?php if( $theme_option['show-copyright'] != 'disable' ){ ?>
		<div class="copyright-wrapper">
			<div class="copyright-container container">
				<div class="copyright-left">
					<?php if( !empty($theme_option['copyright-left-text']) ) echo $theme_option['copyright-left-text']; ?>
				</div>
				<div class="copyright-right">
					<?php if( !empty($theme_option['copyright-right-text']) ) echo $theme_option['copyright-right-text']; ?>
				</div>
				<div class="clear"></div>
			</div>
		</div>
		<?php } ?>
	</footer>
	<?php } // page style ?>
</div> <!-- body-wrapper -->
<?php wp_footer(); ?>

<!-- AddThisEvent theme css -->
<link rel="stylesheet" href="<?php echo get_template_directory_uri()?>/stylesheet/addthisevent.theme7.css" type="text/css" media="screen" />

<!-- AddThisEvent -->
<script type="text/javascript" src="https://addthisevent.com/libs/1.6.0/ate.min.js"></script>

<!-- AddThisEvent Settings -->
<script type="text/javascript">
addthisevent.settings({
    license    : "00000000000000000000",
    mouse      : false,
    css        : false,
    outlook    : {show:true, text:"Outlook"},
    google     : {show:true, text:"Google <em>(online)</em>"},
    yahoo      : {show:true, text:"Yahoo <em>(online)</em>"},
    outlookcom : {show:true, text:"Outlook.com <em>(online)</em>"},
    appleical  : {show:true, text:"Apple Calendar"},
    facebook   : {show:true, text:"Facebook Event"},
    dropdown   : {order:"outlook,google,appleical"},
    callback   : ""
});
</script>
<script type="text/javascript" src="<?php echo get_template_directory_uri().'/javascript/readmore.min.js';?>"></script>
<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery('.gdlr-session-item-readmore').readmore({
		collapsedHeight:45
	});

	//set max height for div in grid view session
	var heights = jQuery(".item-header").map(function ()
    {
        return jQuery(this).height();
    }).get(),

    maxHeight = Math.max.apply(null, heights);

    jQuery('.item-header').height(maxHeight);

	var expert_heights = jQuery(".item-exceprt").map(function ()
    {
        return jQuery(this).height();
    }).get(),

    expert_maxHeight = Math.max.apply(null, expert_heights);

    jQuery('.item-exceprt').height(expert_maxHeight);    
});
</script>


</body>
</html>