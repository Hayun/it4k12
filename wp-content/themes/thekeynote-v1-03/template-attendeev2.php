<?php 
/** Template Name: Attendees Version 2 */
//error_reporting(E_ALL);
//ini_set('display_errors', 'On');
global $post;
require_once('include/database.php');

get_header();
ob_start();
?>
<div id="attendees_content_wrapper">	
<?php
$db = new SDatabase('191.239.4.7', 'dev', 'it4k12345!@#', 'DEV_EIS_RELEASE_COMBINE');
$users = $db->fetchMulti("EXEC sp_executesql 
	N'SELECT [t9].[value] AS [FullName], [t9].[first_name] AS [FirstName], [t9].[last_name] AS [LastName], [t9].[value2] AS [DistrictContactPermissionProfessionalLearning], [t9].[value3] AS [DistrictContactPermissionVideoEvaluator], [t9].[value4] AS [DistrictContactPermissionGeneral], [t9].[value5] AS [DistrictContactPermissionFinancial], [t9].[value6] AS [DistrictContactPermissionPurchasing], [t9].[value7] AS [DistrictContactPermissionLr], [t9].[value8] AS [DistrictContactPermissionVideo], [t9].[value9] AS [DistrictContactPermissionSoftware], [t9].[value10] AS [DistrictContactPermissionOperationalSoftware], [t9].[value11] AS [DistrictContactPermissionAlternate], [t9].[value12] AS [DistrictContactPermissionFrenchImmersion], [t9].[value13] AS [DistrictContactPermissionAboriginalEducation], [t9].[value14] AS [DistrictContactId], [t9].[district_id] AS [DistrictID], [t9].[value15] AS [PositionID], [t9].[value16] AS [PositionName], [t9].[ConferenceTitle], [t9].[value17] AS [DistrictName], [t9].[PhoneWork], [t9].[PhonePersonal], [t9].[D2LAccount], [t9].[Facebook], [t9].[Google], [t9].[Blog], [t9].[LinkedIn], [t9].[Bio], [t9].[Twitter], [t9].[Avatar], [t9].[value18] AS [MainLoginTableId], [t9].[email], [t9].[permissionDistrictContact], [t9].[permissionExecutiveContact], [t9].[permissionLRAContact], [t9].[permissionStaff], [t9].[permissionEvaluator], [t9].[permissionVendor], [t9].[permissionERISAdmin], [t9].[permissionDistrictIndependent], [t9].[active], [t9].[value20] AS [VendorName], [t9].[value19] AS [VendorContactId], [t9].[MainLoginTableId] AS [MainLoginTableId2]
		FROM (
	    SELECT ([t1].[first_name] + @p0) + [t1].[last_name] AS [value], [t1].[first_name], [t1].[last_name], 
	        (CASE 
	            WHEN [t5].[test] IS NULL THEN @p1
	            ELSE CONVERT(Int,[t5].[ProfessionalLearning])
	         END) AS [value2], 
	        (CASE 
	            WHEN [t5].[test] IS NULL THEN @p2
	            ELSE CONVERT(Int,[t5].[VideoEvaluator])
	         END) AS [value3], 
	        (CASE 
	            WHEN [t5].[test] IS NULL THEN @p3
	            ELSE CONVERT(Int,[t5].[General])
	         END) AS [value4], 
	        (CASE 
	            WHEN [t5].[test] IS NULL THEN @p4
	            ELSE CONVERT(Int,[t5].[Financial])
	         END) AS [value5], 
	        (CASE 
	            WHEN [t5].[test] IS NULL THEN @p5
	            ELSE CONVERT(Int,[t5].[Purchasing])
	         END) AS [value6], 
	        (CASE 
	            WHEN [t5].[test] IS NULL THEN @p6
	            ELSE CONVERT(Int,[t5].[Lr])
	         END) AS [value7], 
	        (CASE 
	            WHEN [t5].[test] IS NULL THEN @p7
	            ELSE CONVERT(Int,[t5].[Video])
	         END) AS [value8], 
	        (CASE 
	            WHEN [t5].[test] IS NULL THEN @p8
	            ELSE CONVERT(Int,[t5].[Software])
	         END) AS [value9], 
	        (CASE 
	            WHEN [t5].[test] IS NULL THEN @p9
	            ELSE CONVERT(Int,[t5].[OperationalSoftware])
	         END) AS [value10], 
	        (CASE 
	            WHEN [t5].[test] IS NULL THEN @p10
	            ELSE CONVERT(Int,[t5].[Alternate])
	         END) AS [value11], 
	        (CASE 
	            WHEN [t5].[test] IS NULL THEN @p11
	            ELSE CONVERT(Int,[t5].[FrenchImmersion])
	         END) AS [value12], 
	        (CASE 
	            WHEN [t5].[test] IS NULL THEN @p12
	            ELSE CONVERT(Int,[t5].[AboriginalEducation])
	         END) AS [value13], 
	        (CASE 
	            WHEN [t5].[test] IS NULL THEN @p13
	            ELSE [t5].[DistrictContactId]
	         END) AS [value14], [t1].[district_id], [t1].[PositionID] AS [value15], 
	        (CASE 
	            WHEN [t7].[test] IS NULL THEN CONVERT(NVarChar(50),@p14)
	            ELSE CONVERT(NVarChar(50),[t7].[PositionName])
	         END) AS [value16], [t1].[ConferenceTitle], 
	        (CASE 
	            WHEN [t3].[test] IS NULL THEN CONVERT(NVarChar(100),@p15)
	            ELSE CONVERT(NVarChar(100),[t3].[Name])
	         END) AS [value17], [t1].[PhoneWork], [t1].[PhonePersonal], [t0].[D2LAccount], [t1].[Facebook], [t1].[Google], [t1].[Blog], [t1].[LinkedIn], [t1].[Bio], [t1].[Twitter], [t1].[Avatar], [t1].[MainLoginTableId] AS [value18], [t0].[email], [t0].[permissionDistrictContact], [t0].[permissionExecutiveContact], [t0].[permissionLRAContact], [t0].[permissionStaff], [t0].[permissionEvaluator], [t0].[permissionVendor], [t0].[permissionERISAdmin], [t0].[permissionDistrictIndependent], [t0].[active], [t0].[VendorContactId] AS [value19], [t10].[Title] AS [value20], [t0].[IsDeleted], [t8].[ConferenceID], [t0].[MainLoginTableId]
	    FROM [dbo].[MainLoginTable] AS [t0]
	    INNER JOIN [dbo].[tb_Users] AS [t1] ON ([t0].[MainLoginTableId]) = [t1].[MainLoginTableId]
	    LEFT OUTER JOIN (
	        SELECT 1 AS [test], [t2].[DistrictId], [t2].[Name]
	        FROM [dbo].[District] AS [t2]
	        ) AS [t3] ON [t1].[district_id] = ([t3].[DistrictId])
	    LEFT OUTER JOIN (
	        SELECT 1 AS [test], [t4].[DistrictContactId], [t4].[MainLoginTableId], [t4].[OperationalSoftware], [t4].[Software], [t4].[Video], [t4].[Lr], [t4].[Purchasing], [t4].[Financial], [t4].[General], [t4].[VideoEvaluator], [t4].[ProfessionalLearning], [t4].[Alternate], [t4].[FrenchImmersion], [t4].[AboriginalEducation]
	        FROM [dbo].[DistrictContact] AS [t4]
	        ) AS [t5] ON [t0].[MainLoginTableId] = [t5].[MainLoginTableId]
	    LEFT OUTER JOIN (
	        SELECT 1 AS [test], [t6].[PositionID], [t6].[PositionName]
	        FROM [dbo].[Position] AS [t6]
	        ) AS [t7] ON [t1].[PositionID] = ([t7].[PositionID])
	    LEFT OUTER JOIN [dbo].[Attendees] AS [t8] ON [t0].[MainLoginTableId] = [t8].[MainLoginTableId]
	    LEFT OUTER JOIN [dbo].[VEN_Vendor] AS [t10] ON [t0].[VendorContactId] = [t10].[VendorId]
	    ) AS [t9]
	WHERE ([t9].[active] = 1) AND (NOT ([t9].[IsDeleted] = 1)) AND ([t9].[ConferenceID] = @p16)
	ORDER BY [t9].[first_name]',
	N'@p0 nvarchar(4000),@p1 int,@p2 int,@p3 int,@p4 int,@p5 int,@p6 int,@p7 int,@p8 int,@p9 int,@p10 int,@p11 int,@p12 int,@p13 int,@p14 nvarchar(4000),@p15 nvarchar(4000),@p16 int',
	@p0=N' ',@p1=0,@p2=0,@p3=0,@p4=0,@p5=0,@p6=0,@p7=0,@p8=0,@p9=0,@p10=0,@p11=0,@p12=0,@p13=0,@p14=N'',@p15=N'',@p16=4");
//echo $db->getError();
//		echo '<br>';
//print_r($db->fetchMulti("SELECT [Title],[VendorId] FROM [DEV_EIS_RELEASE_COMBINE].[dbo].[VEN_Vendor]"));
//echo $db->getError();
//exit;
//var_dump($users);
if(count($users)>0) :	

?>
<?php include 'include/content_attendees_v2.phtml'; ?>
</div><!-- #Attendees_content_wrapper -->
<?php
endif;
echo ob_get_clean();
get_footer(); ?>