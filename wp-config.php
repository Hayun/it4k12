<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

/*
 * cPanel & WHM® Site Software
 *
 * Core updates should be disabled entirely by the cPanel & WHM® Site Software
 * plugin, as Site Software will provide the updates.  The following line acts
 * as a safeguard, to avoid automatically updating if that plugin is disabled.
 *
 * Allowing updates outside of the Site Software interface in cPanel & WHM®
 * could lead to DATA LOSS.
 *
 * Re-enable automatic background updates at your own risk.
 */
define('WP_CACHE', true); //Added by WP-Cache Manager
define( 'WPCACHEHOME', '/home2/it4k/public_html/wp-content/plugins/wp-super-cache/' ); //Added by WP-Cache Manager
define( 'WP_AUTO_UPDATE_CORE', false );

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'it4k_wp');

/** MySQL database username */
define('DB_USER', 'it4k_wp');

/** MySQL database password */
define('DB_PASSWORD', 'C)x6@>Hl');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ji0ll81QctCkNQ4sJSam48NFBtWdasZC0GliJskcI_tyPriUNkkngsVgzYmVWhmb');
define('SECURE_AUTH_KEY',  '7WPGJZ0i88W5S1P1fp6TLI5hK_Quluti8ghmZ0Z8brIjFcSFjqK5vkiieTlrLwiR');
define('LOGGED_IN_KEY',    'FvQB3VZ6N5Odvim_cSx5ZyPbKyc6zwPrTA5tu_nM7mxkoRZdp2xExiaKZQaMzrOv');
define('NONCE_KEY',        'yN_Hmg_MELTp07HukP1Hzy2uVLEVbjm5DDsoBnNAb_j6s4QqrBC6fmAPL_q2cqVy');
define('AUTH_SALT',        'yB2cLnhSsOwoCt1A_hS12etGUWyfV1ScxD5JoNOLeNi70un6QQm3v63HLvTriCba');
define('SECURE_AUTH_SALT', 'M9OnJ00QIGbSxtNwl4hlbjoIlePxBcHSiTjcyIH7QCPMZRVv4B6DKgxjedADrcau');
define('LOGGED_IN_SALT',   'kwpRub8KVR0QCBE94RcdxA9jK_K5G6PZ4fWwG0JPwFaHyM3SQtff3iHfFHxMB2YN');
define('NONCE_SALT',       'h2uq7qeQLU4u9jrBerjF_KdnkWu2mww4eL6UR4N6yMjXWLJ3Goq18ZEqx9SJwMTt');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
